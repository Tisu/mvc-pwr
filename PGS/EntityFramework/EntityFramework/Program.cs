﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework2
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new CurseContexta();
            /*using should be here to close context */
            var users = context.m_users.ToList();
            var new_user = new User()
            {
                Id = 8,
                Login = "sda",
                Password = "dsa",

            };
            context.m_users.Add(new_user);
            context.SaveChanges();
            var test = context.m_users
                .Include(x => x.Logins)
                .Where(x => x.Id > 50).ToList();


            foreach (var item in users)
            {
                Console.WriteLine(item.Id);
                Console.WriteLine(item.Login);
                Console.WriteLine(item.Password);
                Console.WriteLine("*****************************");
            }
        }

    }

    public class CurseContexta : DbContext
    {
        public DbSet<User> m_users { get; set; }
        public DbSet<Login> m_login { get; set; }
        public CurseContexta()
        {
            Database.Log = Console.WriteLine;
        }
    }

    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<Login> Logins { get; set; }
    }
    public class Login
    {
        public string Id { get; set; }
        public string Provider { get; set; }
        public string ProviderId { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

    }
}
