﻿namespace Notatki.PWR.Repositories.Dtos
{
    public class NewNoteDto
    {
        public string Content { get; set; }
        public string Type { get; set; }
    }
}