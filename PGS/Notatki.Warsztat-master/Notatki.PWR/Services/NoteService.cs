﻿using System.Linq;
using AutoMapper;
using Notatki.PWR.Controllers;
using Notatki.PWR.Models;
using Notatki.PWR.Repositories;
using Notatki.PWR.Repositories.Dtos;

namespace Notatki.PWR.Services
{
    public class NoteService : INoteService
    {
        private readonly INoteRepository _noteRepository;
        public NoteService(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        public void AddNewNote(AddNoteModel model)
        {
            var note = Mapper.Map<NewNoteDto>(model);
            _noteRepository.CreateNewNote(note);
        }

        public void DeleteNote(int id)
        {

            //var note = _noteRepository.Notes.Single(n => n.Id == id);
            //_noteRepository.Notes.Remove(note);
            //_noteRepository.SaveChanges();

        }

        public void UpdateNote(EditNoteDto model)
        {
            //var note = _noteRepository.Notes.Single(n => n.Id == model.Id);
            //Mapper.Map(model, note);
            //_noteRepository.SaveChanges();

        }

        public EditViewModel GetNoteForEdit(int id)
        {
            //var note = _noteRepository.Notes.Single(n => n.Id == id);
            //var viewmodel = Mapper.Map<EditViewModel>(note);
            //return viewmodel;
            return null;
        }

        public ListNotesViewModel GetNotes()
        {

            //var notes = _noteRepository.Notes.ToList();

            //var viewmodel = Mapper.Map<ListNotesViewModel>(notes);
            //return viewmodel;
             return null;
        }
    }
}