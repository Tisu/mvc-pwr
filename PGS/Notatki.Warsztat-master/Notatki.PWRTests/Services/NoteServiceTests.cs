﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Notatki.PWR.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Notatki.PWR.Controllers;
using Notatki.PWR.Repositories;
using Notatki.PWR.Repositories.Dtos;

namespace Notatki.PWR.Services.Tests
{
    [TestClass()]
    public class NoteServiceTests
    {
        [TestMethod()]
        public void AddNewNoteTest()
        {
            var moqObject = new Moq.Mock<INoteRepository>();
           
            var noteservice = new NoteService(moqObject.Object);
            noteservice.UpdateNote(new EditNoteDto());
            moqObject.Verify(x => x.CreateNewNote(It.IsAny<NewNoteDto>()), Times.Once);
            moqObject.Verify(x => x.CreateNewNote(null), Times.Never);
        }
    }
}