﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TisuController : Controller
    {
        readonly ApplicationDbContext m_db = ApplicationDbContext.Create();

        // GET: Tisu
        public ActionResult Index()
        {
            return View("Game");
        }
        public ActionResult Game()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Game(TisuModel a_model)
        {
            
            if (!ModelState.IsValid)
            {
                return View(a_model);
            }
            m_db.m_game_db.Add(new Entities.Game() { Name = a_model.Name, Price = a_model.Price });
            m_db.SaveChanges();
            return View(a_model);
        }
    }
}