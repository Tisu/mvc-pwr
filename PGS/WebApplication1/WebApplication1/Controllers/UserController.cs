﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class UserController : Controller
    {
        ApplicationDbContext m_db = new ApplicationDbContext();
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult New()
        {
            return View(new UserViewModel());
        }
        [HttpPost]
        public ActionResult New(UserDto a_model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Message = $"Formularz niekompletny! ";
                return View(new UserViewModel());
            }
            m_db.MyUsers.Add(new WebApplication1.Models.User()
            {
                Name = a_model.Name,
                LastName = a_model.LastName,
                Gender = a_model.Gender
            });
            m_db.SaveChanges();
            ViewBag.Message = $"Tworzę użytkownika {a_model.Name}";
            return View(new UserViewModel());
        }
    }
}