﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Entities
{
    public class Game
    {
        [Key]
        public int Id { get; set; }
        [MinLength(5)]
        public string Name { get; set; }
        [Range(0.0, float.MaxValue)]
        public float Price { get; set; }
    }
}