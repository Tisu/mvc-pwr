﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class User
    {
        public User()
        {
            CreateAt = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        [MaxLength(256)]
        public string Name { get; set; }
        [MaxLength(256)]
        public string LastName { get; set; }
        public eSex Gender { get; set; }
        public DateTime CreateAt { get; private set; }
    }
}