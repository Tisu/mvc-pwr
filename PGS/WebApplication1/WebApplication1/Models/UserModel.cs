﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
public enum eSex
{
    Male = 1,
    Female
}
namespace WebApplication1.Models
{
    public class UserDto
    {
        [Required]
        public string Name { get; set; }
        [MinLength(3)]
        public string LastName { get; set; }
        [Required]
        public eSex Gender { get; set; }
    }
}