﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class UserViewModel
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public eSex Gender { get; set; }

        public List<SelectListItem> Genders => new List<SelectListItem>()
        {
            new SelectListItem {Text = "Male", Value = "1" },
            new SelectListItem {Text = "Female", Value ="2" }
        };
    }
}