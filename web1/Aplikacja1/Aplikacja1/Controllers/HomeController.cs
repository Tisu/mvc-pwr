﻿using Aplikacja1.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aplikacja1.Entities;

namespace Aplikacja1.Controllers
{
    //[Authorize(Roles = "AppAdmin")]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        //http://localhost:2089/Home/Test?id=2
        [Route("getmyabout")]
        public ActionResult Test(int id)
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Authorize(Roles = "AppAdmin")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return RedirectToAction("Index");
            return View();
        }
        ApplicationDbContext db = new ApplicationDbContext();
        [HttpGet]
        public ActionResult UploadPhoto()
        {
            return View("PictureUploadView");
        }

        public ActionResult ShowPhotos()
        {
            var picture = db.Pictures.Where(x => x.ImageBytes != null && x.Name != null).ToList();
            //var list = new List<PictureModel>();
            //foreach (var picture in list)
            //{
            //    list.Add(new PictureModel()
            //    {
            //        //property
            //    });
            //} to samo co nizej
            var model = picture.Select(x => new PictureModel()
            {
                Id = x.Id,
                Name = x.Name,
                ImageBytes = x.ImageBytes,
                Likes = x.Likes,
                Comments = x.Comments.Select( comment=> new CommentModel()
                {
                    Message = comment.Message
                }).ToList()
            }).ToList();
            return View("PhotosView", model);
        }
        [HttpPost]
        public ActionResult UploadPhoto(PictureModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("PictureUploadView", model);
            }
            HttpPostedFileBase file;
            if (Request.Files.Count > 0)
            {
                file = Request.Files[0];
                if (file != null && file.ContentLength > 0 && ModelState.IsValid)
                {
                    using (Stream stream = file.InputStream)
                    {
                        MemoryStream memory_stream = stream as MemoryStream;//if cannot convert return null
                        //MemoryStream memory_stream = (MemoryStream)stream ;//throw exception if cannot convert
                        if (memory_stream == null)
                        {
                            memory_stream = new MemoryStream();
                            stream.CopyTo(memory_stream);// when as is correct it copy to automatically
                        }
                        model.ImageBytes = memory_stream.ToArray();
                        memory_stream.Dispose();
                    }
                    var picture = new Picture()
                    {
                        Name = model.Name,
                        ImageBytes = model.ImageBytes
                    };
                    db.Pictures.Add(picture);
                    db.SaveChanges();
                }
            }
            else
            {
                ModelState.AddModelError("Files", "Files are empty");
                return View("PictureUploadView", model);
            }
            //if (!ModelState.IsValid)
            //{
            //    return View("PictureUploadView", model);
            //}
            //db.Pictures.Add(new Entities.Picture() { Name = model.Name });
            return View("PictureUploadView");
        }

        public int LikePhoto(int pictureId)
        {
            var picture = db.Pictures.SingleOrDefault(x => x.Id == pictureId);
            if (picture != null)
            {
                picture.Likes++;
                db.SaveChanges();
                return picture.Likes;
            }
            return 0;
        }
        [HttpGet]
        public ActionResult AddComment()
        {
            return View("AddCommentView");
        }

        [HttpPost]
        public ActionResult AddComment(int pictureId, CommentModel model)
        {
            var picture = db.Pictures.SingleOrDefault(x => x.Id == pictureId);
            if (picture != null)
            {
                var time = DateTime.UtcNow.ToString();
              picture.Comments.Add(new Comment() { Message = model.Message,Time =time,Author = "dsada"});
               db.SaveChanges();
            }
            return RedirectToAction("ShowPhotos");
        }
    }
}