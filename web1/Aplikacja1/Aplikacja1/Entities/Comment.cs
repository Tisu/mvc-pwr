﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Aplikacja1.Entities
{
    [Table("Comments")]
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string Message { get; set; }
        public int Like { get; set; }
        public string Time { get; set; }
        public string Author { get; set; }
        public virtual Picture Picture { get; set; }
    }
}