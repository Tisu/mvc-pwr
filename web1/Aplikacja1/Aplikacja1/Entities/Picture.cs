﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Aplikacja1.Entities
{
    [Table("Pictures")]
    public class Picture
    {
        [Key]
        public int Id { get; set; }
        [StringLength(1024)]
        public string Name { get; set; }
        public byte[] ImageBytes { get; set; }
        public int Likes { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}