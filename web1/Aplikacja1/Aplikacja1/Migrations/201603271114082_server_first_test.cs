namespace Aplikacja1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class server_first_test : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pictures", "Name", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pictures", "Name", c => c.String());
        }
    }
}
