namespace Aplikacja1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pictures : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        Like = c.Int(nullable: false),
                        Picture_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pictures", t => t.Picture_Id)
                .Index(t => t.Picture_Id);
            
            AddColumn("dbo.Pictures", "Likes", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "Picture_Id", "dbo.Pictures");
            DropIndex("dbo.Comments", new[] { "Picture_Id" });
            DropColumn("dbo.Pictures", "Likes");
            DropTable("dbo.Comments");
        }
    }
}
