namespace Aplikacja1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aaaaaaaa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "Time", c => c.String());
            AddColumn("dbo.Comments", "Author", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comments", "Author");
            DropColumn("dbo.Comments", "Time");
        }
    }
}
