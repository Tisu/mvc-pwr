﻿namespace Aplikacja1.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public int Like { get; set; }
        public string Time { get; set; }
        public string Author { get; set; }
    }
}