﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Aplikacja1.Models
{
    public class PictureModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(8, ErrorMessage = "Max 8 letters")]
        [Display(Name = "Name of the picture")]
        public string Name { get; set; }
        public byte[] ImageBytes { get; set; }
        public int Likes { get; set; }
        public List<CommentModel> Comments { get; set; }
    }
}