﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Aplikacja1.Startup))]
namespace Aplikacja1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
