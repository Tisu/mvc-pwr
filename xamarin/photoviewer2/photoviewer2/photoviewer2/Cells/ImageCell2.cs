﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace photoviewer2.Cells
{
    public class ImageCell2 : ViewCell
    {
        public ImageCell2()
        {
            var image = new Image()
            {
                HorizontalOptions = LayoutOptions.Start,
                Aspect = Aspect.AspectFit
            };
            image.SetBinding(Image.SourceProperty, "UrlImageSource");

            var label = new Label()
            {
                TextColor = Color.Lime,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            label.SetBinding(Label.TextProperty, "Title");

            var view_layout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children = { image, label }
            };
            View = view_layout;
        }
    }
}
