﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using photoviewer2.Annotations;
using Xamarin.Forms;

namespace photoviewer2.Models
{
    public class ImageData:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ImageData()
        {
            
        }
        public ImageData(int id, string title, int likes, string imageurl)
        {
            this.Id = id;
            this.Title = title;
            this.Likes = likes;
            this.ImageUrl = imageurl;
            this.UrlImageSource = new UriImageSource()
            {
                Uri = new Uri(this.ImageUrl),
                CacheValidity = new TimeSpan(1, 0, 0),
                CachingEnabled = true
            };
        }
        private int _id;

        
        public int Id
        {
            get { return _id; }
            set
            {
                OnPropertyChanged(nameof(Id));
                _id = value;
            }
        }
        private string _title;

        public string Title
        {
            get { return _title; }
            set
            {
                OnPropertyChanged(nameof(Title));
                _title = value;
            }
        }
        private int _likes;

        public int Likes
        {
            get { return _likes; }
            set
            {
                OnPropertyChanged(nameof(Likes));
                _likes = value;
            }
        }

        private string _imageUrl;

        public string ImageUrl
        {
            get { return _imageUrl; }
            set
            {
                _imageUrl = value;

                this.UrlImageSource = new UriImageSource()
                {
                    Uri = new Uri(value),
                    CacheValidity = new TimeSpan(1, 0, 0),
                    CachingEnabled = true
                };
                OnPropertyChanged(nameof(ImageUrl));
            }
        }
        private UriImageSource _urlImageSource;

        public UriImageSource UrlImageSource
        {
            get { return _urlImageSource; }
            set
            {
                OnPropertyChanged(nameof(UrlImageSource));
                _urlImageSource = value;
            }
        }


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
